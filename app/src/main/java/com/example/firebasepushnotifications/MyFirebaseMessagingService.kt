package com.example.firebasepushnotifications

import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        getFirebaseMessage(p0.notification?.title, p0.notification?.body)
    }

    private fun getFirebaseMessage(title: String?, body: String?) {
        NotificationCompat.Builder() builder : NotificationCompat . Builder (this, "")
        .setSmallIcon(R.drawable.ic_baseline_notifications_active_24)
            .setContentTitle(title)
            .setContentText(body)
    }
}
}